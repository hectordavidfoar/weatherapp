# WeatherApp

Weather App by Hector

## Ambiente

Para compilar el proyecto es necesario instalar una dependencia de CocoaPods. Luego de clonar el repositorio se debe inicializar cocoapods con `pod init`, luego instalar pods con `pod install`. 
Luego abrir el archivo `WeatherApp.xcworkspace`, modificar el archivo Podfile de la siguiente manera para agregar Alamofire:

```text
target 'WeatherApp' do
  use_frameworks!
  pod 'Alamofire', '~> 5.5'
  target 'WeatherAppTests' do
    inherit! :search_paths
  end
  target 'WeatherAppUITests' do
  end
end
````

Luego por Terminal lanzar `pod install` en el directorio del proyecto para instalar la dependencia.

Compilar y probar!!

## Muestra

### Potrait

![Potrait](https://media.giphy.com/media/p0YXjbZA9ML7d6fOKi/giphy.gif)

### Landscape

![Landscape](https://media.giphy.com/media/eyrvefxfqVfLUE0HHX/giphy.gif)

## Consideraciones

* No se tomo en cuenta auto layout para ipad
* No fue incluido la predicción de los dias posteriores debido a limitaciones de tiempo en los proximos dias. Esta caracteristica la tenia pensada implementar mediante un UICollectionView con scroll horizonal en donde se itera una vista para cada dia posterior con su predicción o en su defecto agregar vistas estaticas para cada dia posterior.
* Se agrego en el Launch.storyboard una imagen y un rotulo, sin embargo, no se emplemento un splash customizado por limitaciones de tiempo en los proximos días (segun lo que entiendo en la prueba era una caracteristica que tocaba desarrollar). En este caso para hacerlo se crea un nuevo ViewController con su vista o storyboard adicional para hacer el manejo customizado del "splash" de manera independiente luego de cargar la aplicación
