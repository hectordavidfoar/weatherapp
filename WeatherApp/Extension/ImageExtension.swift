//
//  ImageExtension.swift
//  WeatherApp
//
//  Created by Hector Forero on 5/01/22.
//

import UIKit

extension UIImage {
    /// This fuction download and load image from url
    /// - Parameter url: URL img
    /// - Parameter completion: A block contains data image
    public static func load(url: URL, completion: @escaping (_ image: UIImage?) -> ()) {
        DispatchQueue.global().async {
            if let data = try? Data(contentsOf: url) {
                DispatchQueue.main.async {
                    completion(UIImage(data: data))
                }
            } else {
                DispatchQueue.main.async {
                    completion(nil)
                }
            }
        }
    }
}
