//
//  LocationDetailsViewController.swift
//  WeatherApp
//
//  Created by Hector Forero on 3/01/22.
//

import UIKit

class LocationDetailsViewController: UIViewController {
    
    
    let service = Service()
    let alertService = AlertService()
    var location : Location? = nil
    var consolidated: Consolidated? = nil
    
    @IBOutlet weak var airPressureView: UIView!
    @IBOutlet weak var generalView: UIView!
    @IBOutlet weak var headerView: UIView!
    @IBOutlet weak var imageStatus: UIImageView!
    @IBOutlet weak var tempLabel: UILabel!
    @IBOutlet weak var maxTempLabel: UILabel!
    @IBOutlet weak var minTempLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var windLabel: UILabel!
    @IBOutlet weak var visibilityLabel: UILabel!
    @IBOutlet weak var windValueLabel: UILabel!
    @IBOutlet weak var visibilityValueLabel: UILabel!
    @IBOutlet weak var humidityLabel: UILabel!
    @IBOutlet weak var humidityValueLabel: UILabel!
    @IBOutlet weak var airPressureLabel: UILabel!
    @IBOutlet weak var airPressureValueLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        
        findWoeid(id: location!.woeid!)
        navigationItem.title = location!.title!
 
        
    }
    
    /// Setup view styles
    func setupView() {
        
        let courners: CACornerMask = [.layerMinXMinYCorner, .layerMaxXMinYCorner, .layerMaxXMaxYCorner, .layerMinXMaxYCorner]
        
        headerView.backgroundColor = UIColor(red: 87/255, green: 115/255, blue: 255/255, alpha: 1)
        generalView.backgroundColor = UIColor.white
        generalView.backgroundColor = UIColor.white
        
        headerView.layer.cornerRadius = 15
        generalView.layer.cornerRadius = 15
        airPressureView.layer.cornerRadius  = 15
        
        headerView.layer.maskedCorners = courners
        generalView.layer.maskedCorners = courners
        airPressureView.layer.maskedCorners = courners
        
        
    }
    
    /// Get img from url and set in image view
    func initImageStatus(abbr: String) {
        guard let url = URL(string: "https://www.metaweather.com/static/img/weather/png/\(abbr).png") else {  return }
        
        UIImage.load(url: url) { image in
            self.imageStatus.image = image
        }
    }
    
    /// Set content in view
    func initInformation() {
        
        guard let _consolidated = consolidated else { return }
        
        let today = _consolidated.consolidated_weather.first! // Today
        
        let abbrStatus = today.weather_state_abbr
        
        initImageStatus(abbr: abbrStatus)
        
        let theTemp = Int(round(today.the_temp))
        let maxTemp = Int(round(today.max_temp))
        let minTemp = Int(round(today.min_temp))
        let windValue = Int(round(today.wind_speed))
        let visibilityValue = Int(round(today.visibility))
        let airPressureValue = Int(round(today.air_pressure))
        
        tempLabel.text = "\(theTemp)º"
        statusLabel.text = today.weather_state_name
        maxTempLabel.text = "Max: \(maxTemp)º"
        minTempLabel.text = "Min: \(minTemp)º"
        
        windLabel.text = "Wind"
        windValueLabel.text = "\(windValue) mph"
        humidityLabel.text = "Humidity"
        humidityValueLabel.text = "\(today.humidity)%"
        visibilityLabel.text = "Visibility"
        visibilityValueLabel.text = "\(visibilityValue)%"
        airPressureLabel.text = "Air Pressure"
        airPressureValueLabel.text = "\(airPressureValue)"
    }
    
    /// Find detail from weoid
    func findWoeid(id: Int) {
        let request =  EndpointCases.getWoeid(id: id)
        service.getWoeid(endpoint: request) { (_consolidated, _status, _message) in
            if (_status) {
                guard let consolidated = _consolidated else { return }
                self.consolidated  = consolidated
                self.initInformation()
            } else {
                let alert = self.alertService.createAlert(message: _message)
                self.present(alert, animated: true, completion: nil)
            }
            
        }
    }
    

}

