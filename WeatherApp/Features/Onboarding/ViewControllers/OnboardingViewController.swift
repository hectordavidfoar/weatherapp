//
//  OnboardingViewController.swift
//  WeatherApp
//
//  Created by Hector Forero on 3/01/22.
//

import UIKit

class OnboardingViewController: UIViewController {
    
    static private let storyboardName = "SearchStoryboard"
    static private let storyboardID = "IDSearch"
    
    let service = Service()
    var locations: [Location] = []
    
    @IBOutlet weak var getStartButton: UIButton!
    @IBOutlet weak var bottomView: UIView!
    @IBOutlet weak var bottomTopView: UIView!
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupViews()
        
        let searchLocation = EndpointCases.searchLocation(name: "London")
        
        service.searchLocation(endpoint: searchLocation) { (_locations, _status, _message) in
            guard let locations = _locations else { return }
            self.locations = locations
        }
        
        getStartButton.addTarget(self, action: #selector(handleStartButton), for: .touchUpInside)
    }
        
    /// Setup views style, add corners, color and remove navigation item
    func setupViews() {
        
        getStartButton.accessibilityIdentifier = "getStartedButton"
        view.backgroundColor? = UIColor(red: 243/255, green: 244/255, blue: 253/255, alpha: 1)
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        getStartButton.layer.cornerRadius = 10
        bottomTopView.layer.cornerRadius = 30
        bottomView.layer.cornerRadius = 30
        bottomTopView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        bottomView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        
    }
    
    /// Call navigation to screen search location
    @objc func handleStartButton(sender: UIButton) {
        
        let story = UIStoryboard(name: OnboardingViewController.storyboardName, bundle: nil)
        let searchViewController = story.instantiateViewController(withIdentifier: OnboardingViewController.storyboardID) as! SearchViewController;      self.navigationController?.pushViewController(searchViewController, animated: true)
        
    }
    

}
