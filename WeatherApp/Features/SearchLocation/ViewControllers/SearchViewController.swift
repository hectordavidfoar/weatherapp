//
//  SearchViewController.swift
//  WeatherApp
//
//  Created by Hector Forero on 3/01/22.
//

import UIKit

class SearchViewController: UIViewController {
 

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    var emptyLabel = UILabel()
    
    let service = Service()
    let alertService = AlertService()
    var locations: [Location] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        navigationItem.title  = "Search for city"
        view.backgroundColor = .white
    
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.layoutIfNeeded()
        
        searchBar.delegate = self
        tableView.delegate = self
        tableView.dataSource = self
        
        tableView.separatorStyle = .none
        tableView.layer.backgroundColor = UIColor.clear.cgColor
        tableView.backgroundColor = .clear
        
        registerNewCell()
        initSearchBar()
        addEmptyLabel()
        

    }
    
    /// Init search bar style
    func initSearchBar() {
        searchBar.backgroundImage = UIImage()
        searchBar.placeholder = "Search . . ."
    }
    
    /// Register new cell in table view
    func registerNewCell() {
        let cell = UINib(nibName: "LocationTableViewCell", bundle: nil)
        self.tableView.register(cell, forCellReuseIdentifier: "LocationTableViewCell")
    }
    
    /// Add label when table view is empty
    func addEmptyLabel() {
        emptyLabel = UILabel(frame: CGRect(x: 0, y: 420, width: view.frame.width, height: 30))
        emptyLabel.text = "Without results"
        emptyLabel.font = UIFont.systemFont(ofSize: 30, weight: .bold)
        emptyLabel.textAlignment = .center
        view.addSubview(emptyLabel)
    }
    
    /// Find city when  text change un search bar
    func findCity(name: String) {
        let request = EndpointCases.searchLocation(name: name)
        
        service.searchLocation(endpoint: request) { (_locations, _status, _message) in
            if (_status) {
                guard let locations = _locations else { return }
                self.locations = locations
                self.tableView.reloadData()
            } else {
                let alert = self.alertService.createAlert(message: _message)
                self.present(alert, animated: true, completion: nil)
            }
            
            
        }
        
    }
  
}

extension SearchViewController: UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    static private let storyboardName = "LocationDetailsStoryboard"
    static private let storyboardId = "Details"
    static private let cellId = "LocationTableViewCell"


    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: SearchViewController.cellId) as? LocationTableViewCell {
            cell.titleLabel.text = locations[indexPath.row].title
            cell.typeLabel.text = locations[indexPath.row].location_type
            cell.selectionStyle = .none
            return cell
        }
        return UITableViewCell()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return locations.count
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        let story = UIStoryboard(name: SearchViewController.storyboardName, bundle: nil)
        
        
        let detailsViewController = story.instantiateViewController(withIdentifier: SearchViewController.storyboardId) as! LocationDetailsViewController;
        detailsViewController.location = locations[indexPath.row]
        
        self.navigationController?.pushViewController(detailsViewController, animated: true)

    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        if (!searchText.isEmpty) {
            findCity(name: searchText)
            emptyLabel.removeFromSuperview()
        } else {
            locations = []
            tableView.reloadData()
            view.addSubview(emptyLabel)
        }
    }
}
