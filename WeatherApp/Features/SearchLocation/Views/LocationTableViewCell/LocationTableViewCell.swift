//
//  LocationTableViewCell.swift
//  WeatherApp
//
//  Created by Hector Forero on 3/01/22.
//

import UIKit

class LocationTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    
    @IBOutlet weak var typeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // Add style cell
    override func layoutSubviews() {
        super.layoutSubviews()
        self.contentView.backgroundColor = UIColor(red: 233/255, green: 238/255, blue: 250/255, alpha: 1)
        self.contentView.layer.cornerRadius = 15
        self.contentView.layer.shadowOffset = CGSize(width: 1, height: 0)
        self.contentView.layer.shadowColor = UIColor.gray.cgColor
        self.contentView.layer.shadowRadius = 5
        self.contentView.layer.shadowOpacity = 0.20
        self.contentView.frame = contentView.frame.inset(by: UIEdgeInsets(top: 5, left: 5, bottom: 5, right: 5))

    }
        
}
