//
//  AlertService.swift
//  WeatherApp
//
//  Created by Hector Forero on 5/01/22.
//

import Foundation
import UIKit

class AlertService {
    
    
    func createAlert(message: String) -> UIAlertController {
        let errorAlert = UIAlertController(title: "Error", message: message, preferredStyle: .alert)
        let cancelButton = UIAlertAction(title: "Ok", style: .cancel, handler: nil)
        errorAlert.addAction(cancelButton)
        return errorAlert;
    }
    
}
