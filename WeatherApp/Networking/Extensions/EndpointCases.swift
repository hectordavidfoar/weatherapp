//
//  EndpointCases.swift
//  WeatherApp
//
//  Created by MACUSER on 3/01/22.
//

enum EndpointCases: Endpoint {
    
    case searchLocation(name: String)
    case getWoeid(id: Int)
    
    var baseURL: String {
        switch self {
        case .getWoeid:
            return "https://www.metaweather.com/api/"
        case .searchLocation:
            return "https://www.metaweather.com/api/"
        }
    }
    
    var path: String {
        switch self {
        case .getWoeid(let id):
            return "location/\(id)"
        case .searchLocation:
            return "location/search/"

        }
    }
    
    var param: [String : Any]? {
        switch self {
        case .getWoeid:
            return [:]
        case .searchLocation(let name):
            return ["query": name]
        }
    }
    
    
}
