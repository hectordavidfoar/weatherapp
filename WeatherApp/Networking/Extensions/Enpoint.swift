//
//  Enpoint.swift
//  WeatherApp
//
//  Created by Hector Forero on 3/01/22.
//

protocol  Endpoint {
    var baseURL: String { get }
    var path: String { get }
    var param: [String: Any]? { get }
}

extension Endpoint  {
    var url: String {
        return baseURL + path
    }
}
