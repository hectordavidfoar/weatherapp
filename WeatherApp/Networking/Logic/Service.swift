//
//  Network.swift
//  WeatherApp
//
//  Created by MACUSER on 3/01/22.
//

import Foundation
import Alamofire


class Service {
    
    let worker = Worker();
        
    func searchLocation(endpoint: Endpoint, completion: @escaping ((_ arrayLocation: [Location]?,_ status: Bool,_ message: String) -> Void)) {
        worker.get(endpont: endpoint).response {
            (responseData) in
            guard let data = responseData.data else { return }
            do {
                let locations = try JSONDecoder().decode([Location].self, from: data)
                completion(locations, true, "Success")
            } catch {
                completion(nil, false, error.localizedDescription)
            }
        }
    }
    
    func getWoeid(endpoint: Endpoint, completion: @escaping ((_ data: Consolidated?,_ status: Bool,_ message: String) -> Void)) {
        worker.get(endpont: endpoint).response {
            (reponseData) in
            print(reponseData)
            guard let data = reponseData.data else { return }
            do {
                let consolidated = try JSONDecoder().decode(Consolidated.self, from: data)
                completion(consolidated, true, "Success")
            } catch {
                completion(nil, false, error.localizedDescription)
            }
        }
    }
    
    
    
}
