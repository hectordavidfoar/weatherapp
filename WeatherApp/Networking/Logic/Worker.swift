//
//  Worker.swift
//  WeatherApp
//
//  Created by MACUSER on 3/01/22.
//

import Foundation
import Alamofire

class Worker {

    func get(endpont: Endpoint)  -> DataRequest {
        return AF.request(endpont.url, method: .get, parameters: endpont.param, encoding: URLEncoding.default, headers: nil, interceptor: nil)
    }

    func post(endpont: Endpoint)  -> DataRequest {
        return AF.request(endpont.url, method: .post, parameters: endpont.param, encoding: URLEncoding.default, headers: nil, interceptor: nil)
    }

}
