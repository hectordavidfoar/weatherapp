//
//  Location.swift
//  WeatherApp
//
//  Created by MACUSER on 3/01/22.
//

struct Location: Decodable {
    var title: String?
    var location_type: String?
    var woeid: Int?
    var latt_long: String?
}
