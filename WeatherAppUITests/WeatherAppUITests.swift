//
//  WeatherAppUITests.swift
//  WeatherAppUITests
//
//  Created by MACUSER on 3/01/22.
//

import XCTest

class WeatherAppUITests: XCTestCase {

    override func setUpWithError() throws {
        super.setUp()
        continueAfterFailure = false
        XCUIApplication().launch()
    }

    override func tearDownWithError() throws {
    }
    
    
    func testButtonGetStarted() throws {
        let app = XCUIApplication()
        let button = app.buttons["getStartedButton"]
        button.tap()
    }

    func testLaunchPerformance() throws {
        if #available(macOS 10.15, iOS 13.0, tvOS 13.0, watchOS 7.0, *) {
            measure(metrics: [XCTApplicationLaunchMetric()]) {
                XCUIApplication().launch()
            }
        }
    }
}
